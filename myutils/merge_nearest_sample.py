import pandas as pd


def merge_nearest_sample(
    left: pd.DataFrame,
    right: pd.DataFrame,
    dhid: str = "bhid",
    ifrom: str = "from",
    ito: str = "to",
    tolerance: float = 1.5,
) -> pd.DataFrame:
    """Merge right sample table with left by nearest downhole depth.

    Alex Trueman, 2019-08-01

    Useful for merging sample tables (tables with dhid, from, and to depth)
    where the from and to depths in the two table do not match. Rather than
    creating new records in the left table, the nearest record from the right
    table is assigned.This method can cause data loss and should be checked.

    Parameters
    ----------
    left : DataFrame to be updated, must have dhid, from, and to depths.
    right : DataFrame to merge, must have dhid, from, and to depths.
    dhid : Name of the hole identification column.
    ifrom : Name of the downhole sample from-depth column.
    ito : Name of the downhole sample to-depth column.
    tolerance : Tolerance parameter for finding nearest sample. Can be
        though of as a downhole distance within which a nearest sample
        must be found. This parameter should be tested for data loss.

    Return
    ------
    The left DataFrame is returned with the addition of columns from the
    right DataFrame.

    """
    c_left = left.copy()
    c_right = right.copy()

    # Calculate sample mid-points.
    mid_samp = lambda df, ifrom, ito: df[ifrom] + (df[ito] - df[ifrom]) / 2
    c_left["mid_samp"] = mid_samp(c_left, ifrom, ito)
    c_right["mid_samp"] = mid_samp(c_right, ifrom, ito)

    # Prepare for merging.
    c_right.drop(columns=[ifrom, ito], inplace=True)
    c_right.sort_values(by="mid_samp", inplace=True)
    c_left.sort_values(by="mid_samp", inplace=True)

    # Merge by nearest midpoint and by hole id.
    df = (
        pd.merge_asof(
            c_left,
            c_right,
            on="mid_samp",
            by=dhid,
            tolerance=tolerance,
            direction="nearest",
        )
        .drop(columns=["mid_samp"])
        .sort_values(by=[dhid, ifrom, ito])
    )

    return df
