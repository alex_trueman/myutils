import numpy as np
import pandas as pd


def cols_unique(data: pd.DataFrame, cols: list) -> np.ndarray:
    """Find unique values across multiple columns (of same type)

    Alex M Trueman, 2019-07-12

    This function looks at the values in all the listed columns and
    creates a sorted array of their unique values. `NaNs` are not
    included in this list.
    """

    # Get 1D array of each column's unique values.
    uvals = np.concatenate(tuple(data[i].unique() for i in cols), axis=0)
    # Remove `NaN`s. Use `pd.isnull` as more robust to object (string)
    # type columns with `NaN` values.
    uvals = uvals[~pd.isnull(uvals)]
    # Get the unique values in the array of column unique values.
    # `np.unique` sorts the output.
    uvals = np.unique(uvals)

    return uvals
