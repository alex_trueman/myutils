import pandas as pd
from pygeostat import DataFile, GridDef
from typing import Union, Sequence


def griddef_from_xyz(
    grid: Union[pd.DataFrame, DataFile],
    size: Sequence[float] = [1, 1, 1],
    coords: Sequence[str] = ["x", "y", "z"],
) -> GridDef:
    """Calculate GSLIB grid definition from xyz block model.

    The input grid is assumed to have a single block size (i.e., no sub-blocks)
    as the output pygeostat GridDef does not support this.

    Parameters
    ----------
    grid : A DataFrame or DataFile defining a grid with x, y, and z-coordinate
        columns and other attributes.
    size : A list of grid node spacings (block sizes) for the x, y, and z-axes.
        Must be the same length as `coords`, which is either 2 or 3..
    coords: A list of column names for the x, y, and z-axes. If only two items
        supplied, the grid is assumed to be 2D. Valid lengths for this list
        are 2 or 3 only and must be the same length as `size`.
    """

    if not (isinstance(grid, pd.DataFrame) or isinstance(grid, DataFile)):
        raise Exception("`grid` must be a DataFrame or DataFile.")
    if len(size) != len(coords):
        raise Exception("Parameters `size` and `coords` must be the same length.")
    if len(coords) < 2 or len(coords) > 3:
        raise Exception("Permissible length for `coords` is 2 or 3 only.")
    if not all(col in grid.columns for col in coords):
        raise Exception("Not all `coords` are columns in `grid`.")
    if not all(x > 0 for x in size):
        raise Exception("Values of `size` must be positive and non-zero numbers.")

    # Handle DataFile.
    if isinstance(grid, DataFile):
        grid = grid.data

    # Calculate the grid parameters assuming 2D or 3D based on number of
    # coordinate column names passed to `coords`.
    if len(coords) == 3:
        xmn, ymn, zmn = (
            grid[coords[0]].min(),
            grid[coords[1]].min(),
            grid[coords[2]].min(),
        )
        nx, ny, nz = (
            int((grid[coords[0]].max() - xmn) / size[0]),
            int((grid[coords[1]].max() - ymn) / size[1]),
            int((grid[coords[2]].max() - zmn) / size[2]),
        )
        griddef = GridDef(
            gridarr=[nx, xmn, size[0], ny, ymn, size[1], nz, zmn, size[2]]
        )
    elif len(coords) == 2:
        xmn, ymn = (grid[coords[0]].min(), grid[coords[1]].min())
        nx, ny = (
            int((grid[coords[0]].max() - xmn) / size[0]),
            int((grid[coords[1]].max() - ymn) / size[1]),
        )
        griddef = GridDef(gridarr=[nx, xmn, size[0], ny, ymn, size[1], 1, 0.5, 1])

    return griddef