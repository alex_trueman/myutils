import pandas as pd
import pygeostat as gs
from typing import Union, Tuple


def compare_samp_tables(
    samp1: Union[pd.DataFrame, gs.DataFile],
    samp2: Union[pd.DataFrame, gs.DataFile],
    var: Union[str, Tuple[str, str]] = None,
    dhid: Union[str, Tuple[str, str]] = "bhid",
    ifrom: Union[str, Tuple[str, str]] = "from",
    ito: Union[str, Tuple[str, str]] = "to",
) -> dict:
    """Compare two drillhole files for validation.

    Some useful comparisons of sample statistics for validations after
    operations on the data such as compositing, desurveying, etc.

    Parameters
    ----------
    samp1 : Sample data for comparison.
    samp2 : Sample data for comparison.
    var : Optional grade column for comparing accumulation and grade. Can be a
        two-item tuple if different in `samp1` and `samp2`. `None` if not
        comparing grade column.
    dhid : Drillhole ID column. Can be a two-item tuple if different in `samp1`
        and `samp2`.
    ifrom : Downhole depth to start of sample. Can be a two-item tuple if
        different in `samp1` and `samp2`.
    ito : Downhole depth to end of sample. Can be a two-item tuple if different
        in `samp1` and `samp2`.

    Return
    ------
    A dictionary of comparison statistics.
    """

    # Handle pygeostat.DataFile inputs.
    if isinstance(samp1, gs.DataFile):
        samp1 = samp1.data
    if isinstance(samp2, gs.DataFile):
        samp2 = samp2.data

    # Handle samp1/samp2 parameters. Convert all to two-item tuple.
    if not isinstance(var, tuple):
        var = (var, var)
    if not isinstance(dhid, tuple):
        dhid = (dhid, dhid)
    if not isinstance(ifrom, tuple):
        ifrom = (ifrom, ifrom)
    if not isinstance(ito, tuple):
        ito = (ito, ito)

    # Get only required columns to prevent removing too many records when
    # removing NaNs.
    samp1 = samp1.loc[
        :, [v for v in [dhid[0], ifrom[0], ito[0], var[0]] if v is not None]
    ]
    samp2 = samp2.loc[
        :, [v for v in [dhid[1], ifrom[1], ito[1], var[1]] if v is not None]
    ]
    # Remove row with an NaN value.
    samp1 = samp1.dropna()
    samp2 = samp2.dropna()

    # Count unique drillholes and non-NaN samples.
    samp1_dh_count = samp1[dhid[0]].unique().shape[0]
    samp2_dh_count = samp2[dhid[1]].unique().shape[0]
    samp1_samp_count = samp1.shape[0]
    samp2_samp_count = samp2.shape[0]

    # Accumulate non-NaN sample length and compare.
    samp1_sum_len = round(sum(samp1[ito[0]] - samp1[ifrom[0]]))
    samp2_sum_len = round(sum(samp2[ito[1]] - samp2[ifrom[1]]))

    # Accumulate 'metal' if `var`.
    samp1_sum_metal = None
    samp2_sum_metal = None
    samp1_mean_grade = None
    samp2_mean_grade = None
    if var[0] is not None:
        samp1_sum_metal = round(sum((samp1[ito[0]] - samp1[ifrom[0]]) * samp1[var[0]]))
        samp2_sum_metal = round(sum((samp2[ito[1]] - samp2[ifrom[1]]) * samp2[var[1]]))
        samp1_mean_grade = round(samp1_sum_metal / samp1_sum_len, 1)
        samp2_mean_grade = round(samp2_sum_metal / samp2_sum_len, 1)

    report = dict(
        samp1_drillholes=samp1_dh_count,
        samp2_drillholes=samp2_dh_count,
        samp1_samples=samp1_samp_count,
        samp2_samples=samp2_samp_count,
        samp1_length=samp1_sum_len,
        samp2_length=samp2_sum_len,
        samp1_accum=samp1_sum_metal,
        samp2_accum=samp2_sum_metal,
        samp1_grade=samp1_mean_grade,
        samp2_grade=samp2_mean_grade,
    )
    report = {k: v for k, v in report.items() if v is not None}

    return report
