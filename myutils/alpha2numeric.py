import pandas as pd
from typing import Tuple


def alpha2numeric(
    data: pd.DataFrame,
    alphacol: str = "bhid",
    numcol: str = "id",
    tofront: bool = True,
    drop: bool = True,
) -> Tuple[pd.DataFrame, dict]:
    """Convert an alphanumeric categorical column to numeric codes.

    Alex M Trueman, 2019-07-10

    Automatically assigns numeric codes replacing alphanumeric codes.
    This is the form expected by GSLIB-style programs, which don't work
    with alphanumeric fields. Useful for hole id column and other
    alphanumeric columns typically encountered in data. The function
    returns a dictionary, which maps the numeric code back to the
    alphanumeric code. This can be used to re-assign the alphanumeric
    code at a later time e.g.,:
        `df.replace({"id": dhdict})`

    Parameters
    ----------
    df: DataFrame containing column with alphanumeric values.
    alphacol: Name of the alphanumeric categorical column in `df`.
    numcol: Name of the numeric column to be created. This will
        overwrite any existing column with same name.
    tofront: If `True`, move the column to be the first in `df`.
    drop: If `True`, drop the alphanumeric column from `df`.

    Returns
    -------
    Tuple with 0: modified DataFrame and 1: dictionary mapping the new
    numeric codes (key) to the original alphanumeric codes (value).

    Example
    -------

    import pandas.util.testing as tm

    # Set default rows and columns for Pandas testing utilities.
    tm.N, tm.K = 5, 3

    # Make a dummy DataFrame
    df = tm.makeDataFrame()\
        .set_index(tm.makeCategoricalIndex(k=5, name="bhid"))\
        .reset_index()

    # Convert the `bhid` column to numeric.
    dtfl, dhdict = alpha2numeric(df)

    print("Original DataFrame:\n", df, "\n")
    print("Updated DataFrame:\n", dtfl, "\n")
    print("Hole id dictionary:\n", dhdict)

    """

    # Copy the input DataFrame to prevent overwriting by reference.
    df = data.copy()

    # Convert column to numeric.
    df[alphacol] = pd.Categorical(df[alphacol])
    df[numcol] = df[alphacol].cat.codes

    # Create dictionary linking the numeric and alphanumeric fields.
    map_dict = pd.Series(df[alphacol].values, index=df[numcol]).to_dict()

    if drop:
        df.drop(columns=[alphacol], inplace=True)

    if tofront:
        cols = list(df)
        # Move the column to head of list using index, pop and insert
        cols.insert(0, cols.pop(cols.index(numcol)))
        # Use loc to reorder
        df = df.loc[:, cols]

    return df, map_dict
