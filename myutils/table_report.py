import pandas as pd


def table_report(
    df: pd.DataFrame,
    nan_cols: list = None,
    zero_cols: list = None,
    keys: list = ["bhid"],
    sample: bool = False,
    bhid: str = "bhid",
    ifrom: str = "from",
    ito: str = "to",
) -> dict:
    """Drillhole table validation reports per table.

    Alex M Trueman, 2019-07-05

    These are simple checks of the tables used to build drillholes. See
    the section *Returns* at the end of this documentation for a detailed
    description of the reported statistics.

    Args:
    ----------

    df Drillhole table, usually collars, surveys, or some sample type.
    nan_cols List of column names to check for missing values.
    zero_cols Check for zero values and for negative values (must be
        numeric columns, but I don't check for this).
    keys list of column names to use as keys for duplicate checking.
    sample Is this a sample file with `from` and `to` depths?
    bhid Name of hole id column
    ifrom name of from depth column
    ito name of to depth column

    Returns:
    -------------

    Returns a dictionary of validation statistics depending on the input
    drillhole table type.

        - count: number of records in table including NaN and other
            issues.
        - duplicates: number of records having identical values for
        columns defined in the `keys` argument.
        - nan_values: number of records with any `NaN` value in the
        columns defined in the `nan_cols` argument.
        - zero_values: number of records with any zero value in the
        columns defined in the `zero_cols` argument.
        - negative_values: number of records with any negative value
        in the columns defined in the `zero_cols` argument.

    The following are checked only if `sample` is `True`:

        - fromto_errors: Number of records where `to` is less than
            `from`.
        - gaps: Number of records where `to` is less than the next
            `from` meaning there is a gap.
        - overlaps: Number of records where `to` is greater than the
            next `from` meaning there is an overlap.
    """

    def report_nans(df, nan_cols):
        """Count records with any NaNs for listed columns."""
        if nan_cols is None:
            return None
        count_nans = df[nan_cols].shape[0] - df[nan_cols].dropna().shape[0]
        return count_nans

    def report_zero(df, zero_cols):
        """Count records with any zero value for listed columns."""
        if zero_cols is None:
            return None
        count_zero = (
            df[zero_cols].shape[0] - df[~(df[zero_cols] == 0).any(axis=1)].shape[0]
        )
        return count_zero

    def report_neg(df, neg_cols):
        """Count records with any negative for listed columns."""
        if zero_cols is None:
            return None
        count_neg = df[neg_cols].shape[0] - df[~(df[neg_cols] < 0).any(axis=1)].shape[0]
        return count_neg

    # Capture some table statistics.
    tbl_cnt = df.shape[0]
    tbl_dup = df.duplicated(subset=keys).sum()
    tbl_nan = report_nans(df, nan_cols)
    tbl_zero = report_zero(df, zero_cols)
    tbl_neg = report_neg(df, zero_cols)

    # If sample table, check for incorrect from/to, gaps, and
    # overlapping samples.
    tbl_tof = None
    tbl_gap = None
    tbl_ovl = None
    if sample:
        df.sort_values([bhid, ifrom, ito], inplace=True)
        tbl_tof = df[df[ito] <= df[ifrom]].shape[0]
        tbl_gap = sum(
            (df[bhid] == df[bhid].shift(-1, axis=0))
            & (df[ito] < df[ifrom].shift(-1, axis=0))
        )
        tbl_ovl = sum(
            (df[bhid] == df[bhid].shift(-1, axis=0))
            & (df[ito] > df[ifrom].shift(-1, axis=0))
            & ~df.duplicated(subset=keys, keep=False)
        )

    # Make dictionary of validation statitics.
    full = dict(
        count=tbl_cnt,
        duplicates=tbl_dup,
        nan_records=tbl_nan,
        zero_records=tbl_zero,
        negative_records=tbl_neg,
        fromto_errors=tbl_tof,
        gaps=tbl_gap,
        overlaps=tbl_ovl,
    )
    # Only make dictionary entry if value is not `None`.
    report = {k: v for k, v in full.items() if v is not None}

    return report
