import pandas as pd
from .antijoin import antijoin


def table2table_check(
    collars: pd.DataFrame,
    surveys: pd.DataFrame,
    samples: dict = None,
    cols: dict = {"id": "bhid", "at": "at"},
) -> dict:
    """Validations between drillhole tables.

    Alex M Trueman, 2019-07-10

    Some simple checks of associations between drillhole tables. See the
    section *Returns* (below) for details on the statistics generated.

    Args:
    ----------
    collars Drillhole collars table.
    surveys Drillhole downhole surveys table
    [samples] Dictionary of sample table names. Key is string version of
        table name. Value is DataFrame, e.g., {'assays': assays}.
    cols Dictionay of standard drillhole column names. 'id' is the
        drillhole id and defaults to 'bhid'. 'at' is the downhole depth
        used in the survey file and defaults to 'at'. Change the values
        here if your data has different column names.

    Returns:
    ----------
    Returns a dictionary of counts for various checks:

    * Collars table hole id with no matching survey table entry.
    * No survey at 0 depth (no survey at collar).
    * Survey table hole id with not matching collar table entry.
    * Collar table hole id with no matching sample table entry for each
        passed sample table.
    * Sample table hole id with no matching collar table entry for each
        passed sample table.
    """

    # Column names are defined in a dictionary for future versions that
    # might do other checks of other column/table types.

    # Sample table names are passed as a dictionary, rather than as a
    # list, so that the table name, passed as the key, can be included
    # in the reported statistics.

    coll_no_svy = antijoin(collars, surveys, [cols["id"]]).shape[0]
    no_svy_at_coll = (
        antijoin(collars, surveys[surveys[cols["at"]] == 0], [cols["id"]])
        .bhid.unique()
        .shape[0]
    )
    svy_no_coll = antijoin(surveys, collars, ["bhid"]).shape[0]

    # If there are sample tables.
    if not samples is None:
        coll_no_samp = {}
        samp_no_coll = {}
        for k in samples.keys():
            coll_no_samp[k] = (
                antijoin(collars, samples[k], [cols["id"]])[cols["id"]]
                .unique()
                .shape[0]
            )
            samp_no_coll[k] = antijoin(samples[k], collars, [cols["id"]]).shape[0]
    else:
        coll_no_samp = None
        samp_no_coll = None

    # Build a dictionary to return.
    report = {
        "collar no survey": coll_no_svy,
        "no survey at collar": no_svy_at_coll,
        "survey no collar": svy_no_coll,
        "collar no sample": coll_no_samp,
        "sample no collar": samp_no_coll,
    }
    # Remove untested stats.
    report = {k: v for k, v in report.items() if v is not None}

    return report
