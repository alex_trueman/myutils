import numpy as np


def weighted_stats(values, weights):
    """ Return the weighted average and standard deviation.

    Modified from: https://stackoverflow.com/a/2415343/4516267

    """

    mean = np.average(values, weights=weights)
    variance = np.average((values - mean) ** 2, weights=weights)
    std = np.sqrt(variance)
    cv = std / mean

    return mean, std, variance, cv
