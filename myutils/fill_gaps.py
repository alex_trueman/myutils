import pandas as pd
from typing import Union
from pygeostat import DataFile

def fill_gaps(
    data: Union[pd.DataFrame, DataFile],
    bhid: str = "bhid",
    ifrom: str = "from",
    ito: str = "to"
) -> Union[pd.DataFrame, DataFile]:
    """Fill gaps in a drillhole sample table.

    Alex M Trueman, 2019-07-01

    Create new records with columns of input DataFrame setting all but
    the `bhid`, `from`, and `to` to `NaN`. Set the `from` to the `to` of
    the current record and the `to` to the `from` of the next record.
    Append the new records, sort, and reset the index.

    Parameters
    ----------
    data : Sample-type data.
    bhid : Name of the hole id columns in `data`.
    ifrom : Name of the depth to start of sample column in `data`.
    ito : Name of the depth to end of sample column in `data`.

    Return
    ------
    Object of same type as `data` with same columns in same order. New
    records added to fill gaps between samples in the same drillhole.
    """

    # Handle DataFiles.
    if isinstance(data, DataFile):
        df = data.data.copy()
    else:
        df = data.copy()

    # Capture column order of input.
    cols = df.columns

    # Sort for this to work.
    df.sort_values([bhid, ifrom, ito], inplace=True)

    # Create column of next `from`.
    df["newto"] = df[ifrom].shift(-1, axis=0)
    # Filter out records that have a gap after.
    df_gap = df.loc[(df[bhid] == df[bhid].shift(-1, axis=0)) & (df[ito] < df["newto"])]

    # Fix the `from`s and `to`s.
    df_gap.loc[:, ifrom] = df_gap[ito]
    df_gap.loc[:, ito] = df_gap["newto"]
    df_gap = df_gap.loc[:, [bhid, ifrom, ito]]

    # Add fixed gaps back into the original DataFrame.
    dfout = pd.concat([df, df_gap], sort=True)
    dfout.sort_values([bhid, ifrom, ito], inplace=True)

    # Preserve column  order of input. Remove temporary columns.
    dfout = dfout[cols]

    # Return DataFile if input was.
    if isinstance(data, DataFile):
        data.data = dfout.copy()
        return data
    else:
        return dfout
