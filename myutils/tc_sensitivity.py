import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import pygeostat as gs
from typing import Union, Tuple
from .weighted_stats import weighted_stats


def tc_sensitivity(
    data: Union[pd.DataFrame, gs.DataFile],
    var: str,
    weight: str = None,
    logx: bool = False,
    title: str = None,
    color: str = "steelblue",
    figsize: Tuple[float, float] = (10, 8),
) -> mpl.axes.Axes:
    """Mean and CV top cut sensitivity plots.

    Produces a set of four subplots for extreme value analysis:
        - The histogram, optionally with log x-axis.
        - The CDF, optionally with log x-axis.
        - Plot of mean grade versus top cut.
        - Plot of coefficient of variation (CV) versus top cut.

    Parameters
    ----------
    data : containing `var` and optionally `weight`.
    var : Column name for variable of interest.
    weight : Column name for weighting variable.
    logx : Log 10 scale for histogram and CDF x-axes.
    title : Title above the four plots.
    colour : Color for the plot aesthetics.
    figsize : Figure size in inches.


    """

    # Handle DataFiles.
    if isinstance(data, gs.DataFile):
        data = data.data

    # Top cuts from median in 0.5 percentile increments to 99th and then
    # 0.1 percentiles up to 99.9th.
    topcuts = np.percentile(
        data[var], np.concatenate((np.linspace(75, 99, 49), np.linspace(99.1, 99.9, 9)))
    )

    # Weights are optional, set all to 1 if None supplied.
    nrecs = data.shape[0]
    weights = np.full(nrecs, 1.0) if weight is None else data[weight].values

    # Generate stats for the listed top cuts.
    tc_mean = np.empty(len(topcuts))
    tc_cv = np.empty(len(topcuts))
    for i, tc in enumerate(topcuts):
        dftc = data[[var]].clip(upper=tc).values.flatten()
        tc_mean[i], _, _, tc_cv[i] = weighted_stats(dftc, weights=weights)

    # Mean and CV of uncut data.
    uc_mean, _, _, uc_cv = weighted_stats(data[[var]].values.flatten(), weights=weights)

    # Add data point for maximum value to complete the curve.
    max_val = max(data[[var]].values.flatten())
    topcuts = np.append(topcuts, max_val)
    tc_mean = np.append(tc_mean, uc_mean)
    tc_cv = np.append(tc_cv, uc_cv)

    # Make the  plots.
    fig, axes = plt.subplots(2, 2, figsize=figsize)
    ax = axes.flatten()
    gs.histplt(
        data=data,
        var=var,
        bins=30,
        logx=logx,
        ax=ax[0],
        xlabel="Grade",
        title="Log Histogram" if logx else "Histogram",
        stat_blk=False,
        color=color,
    )
    gs.histplt(
        data=data,
        var=var,
        bins=100,
        logx=logx,
        icdf=True,
        ax=ax[1],
        xlabel="Grade",
        title="Log CDF" if logx else "CDF",
        stat_blk=False,
        color=color,
    )
    ax[2].plot(topcuts, tc_mean, color=color, label="Sensitivity curve")
    ax[3].plot(topcuts, tc_cv, color=color, label="Sensitivity curve")
    ax[2].set_ylabel("Top cut mean")
    ax[2].set_xlabel("Top cut")
    ax[3].set_ylabel("Top cut CV")
    ax[3].set_xlabel("Top cut")
    ax[2].grid(which="both")
    ax[3].grid(which="both")
    ax[2].set_title("Mean Sensitivity")
    ax[3].set_title("CV Sensitivity")
    ax[2].axhline(uc_mean, color=color, linestyle="--", label="Uncut mean")
    ax[3].axhline(uc_cv, color=color, linestyle="--", label="Uncut CV")
    ax[2].legend(loc="lower right")
    ax[3].legend(loc="lower right")

    if title is not None:
        fig.suptitle(title, fontweight="bold")
        fig.tight_layout(rect=[0, 0, 1, 0.97])
    else:
        fig.tight_layout()

    return ax
