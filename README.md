# myutils

Just a bunch of functions I use -- a real hodgepodge. As I learn more I will organize this a bit better, either in sub-modules or in separate module.

Most of these functions are specific to mineral resource evaluation and geostatistics, but some might be useful elsewhere. The CCG python module `pygeostat` is needed for some of its functions and to handle the `pygeostat.DataFile` object type.

I usually import as `my`:

```python
import myutils as my
```

Use at your own risk!